export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'quest',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name:"google-site-verification", content:"9d_snRf7uo5R0vqWPQNS1rA7AiOWTONp3xqflsG6NqQ" },
      { name:"yandex-verification", content:"56d8525c23a9612c"},
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel:"stylesheet", href:"https://fonts.googleapis.com/css2?family=Lobster&display=swap"},
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    '@nuxtjs/pwa',
  ],
  pwa: {
    workbox: {
      runtimeCaching: [
        {
          urlPattern: 'https://quest-a54bd.web.app/.*',
          handler: 'cacheFirst',
          method: 'GET',
          strategyOptions: {
            cacheName: 'my-api-cache',
            cacheableResponse: {statuses: [0, 200]}
          }
        }
      ]
    }
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/firebase',
  ],
  firebase:{
    config:{
      apiKey: "AIzaSyBIMNtHSEMcPwTVuXjl4TlJ6rX7vU3MruQ",
      authDomain: "quest-a54bd.firebaseapp.com",
      databaseURL: "https://quest-a54bd-default-rtdb.europe-west1.firebasedatabase.app",
      projectId: "quest-a54bd",
      storageBucket: "quest-a54bd.appspot.com",
      messagingSenderId: "297127651784",
      appId: "1:297127651784:web:bd76ba607141f79dd938fd",
      measurementId: "G-6CVNFBYVNF"
    },
    services: {
      auth: true,
      database:true,
    }
  },

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
  },
  loading: {
    color: 'blue',
    height: '5px'
  }
}
